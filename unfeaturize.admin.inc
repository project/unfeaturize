<?php
/**
 * @file
 * Administration pages for Unfeaturize.
 */

/**
 * Administration form.
 */
function unfeaturize_admin_form($form, &$form_state) {
  $defaults = array(
    'features' => isset($form_state['values']['features'])? $form_state['values']['features'] : array(),
    'disable' => isset($form_state['values']['disable'])? $form_state['values']['disable'] : TRUE,
  );

  $rows = array();

  $default_text = array(
    FEATURES_OVERRIDDEN => t('Overridden'),
    FEATURES_DEFAULT => t('Default'),
    FEATURES_NEEDS_REVIEW => t('Needs review'),
    FEATURES_REBUILDING => t('Rebuilding'),
    FEATURES_REBUILDABLE => t('Rebuilding'),
    FEATURES_CONFLICT => t('Conflict'),
    FEATURES_DISABLED => t('Disabled'),
    FEATURES_CHECKING => t('Checking...'),
  );

  $form['modules'] = array(
    '#type' => 'value',
    '#value' => array(),
  );
  $options = array();

  // Get all features modules.
  module_load_include('inc', 'features', 'features.admin');
  $features = _features_get_features_list();
  foreach ($features as $machine => $feature) {

    $form['modules']['#value'][$machine] = $machine;
    $options[$machine] = $feature->info['name'];

    // Sort $feature->components into supported and unsupported. Indicate here
    // which is which. @todo

    // Only include enabled features.
    if ($feature->status == 1) {
      $row = array(
        'name' => $feature->info['name'],
        'machine' => $machine,
        'state' => $default_text[$feature->status],
        'components' => implode(', ', $feature->components),
        'description' => $feature->info['description'],
      );

      $rows[$machine] = $row;
    }
  }

  $header = array(
    t('Restore'),
    t('Name'),
    t('Machine name'),
    t('Storage state'),
    t('Components'),
    t('Description'),
  );

  $form['features'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which features would you like to restore into the database?'),
    '#options' => $options,
    '#header' => $header,
    '#rows' => $rows,
    '#theme' => 'unfeaturize_admin_table',
    '#default_value' => $defaults['features'],
  );

  $defaults['disable'] = FALSE;
  $form['disable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Would you like to disable these features at the same time?'),
    '#default_value' => $defaults['disable'],
  );

  $form['actions'] = array(
    '#type' => 'container',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('UnFeaturize'),
    ),
  );

  // Change the submit button text.
  $form['actions']['submit']['#value'] = t('UnFeaturize');

  return $form;
}

/**
 * Submit handler for administration form.
 */
function unfeaturize_admin_form_submit(&$form, &$form_state) {
  // Loop through each feature.
  $disable_me = array();
  foreach ($form_state['values']['features'] as $machine => $restore_me) {
    if ($restore_me) {
      // Disable the feautre, if requested.
      if ($form_state['values']['disable']) {
        $disable_me[] = $form_state['values']['modules'][$machine];
      }

      // Restore the feature to the database.
      if ($machine) {
        unfeaturize_restore($machine);
        drupal_set_message(t('The feature <em>!feature</em> has been restored in the database.', array('!feature' => $machine)));
      }
    }
  }

  if ($form_state['values']['disable'] && !empty($disable_me)) {
    module_disable($disable_me);
    $modules = implode(', ', $disable_me);
    drupal_set_message(t('The following modules have been disabled: <em>!modules</em>', array('!modules' => $modules)));
  }

  // Clean Drupal cache for the "Delete" link to appear for the content type
  // that was created by the feature.
  drupal_flush_all_caches();
}

