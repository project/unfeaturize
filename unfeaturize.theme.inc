<?php
/**
 * @file
 * Theme functions for unfeaturize.
 */

/**
 * Theme function: styles the admin table.
 */
function theme_unfeaturize_admin_table($variables) {
  $element = $variables['element'];

  $header = $element['#header'];
  $rows = $element['#rows'];
  $options = $element['#options'];

  $children_keys = element_children($element);

  foreach ($rows as $module => &$row) {
    if (in_array($module, $children_keys)) {
      $element[$module]['#title_display'] = 'invisible';
      $checkbox = drupal_render($element[$module]);
      array_unshift($row, $checkbox);
    }
    else {
      array_unshift($row, '&nbsp;');
    }
  }

  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  $attributes['class'][] = 'form-checkboxes';
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = array_merge($attributes['class'], $element['#attributes']['class']);
  }
  if (isset($element['#attributes']['title'])) {
    $attributes['title'] = $element['#attributes']['title'];
  }

  $empty = t('Congratulations! There are no features left to unfeaturize.');

  $output = '<div' . drupal_attributes($attributes) . '>';
  $output .=  theme('table', array('header' => $header, 'rows' => $rows, 'empty' => $empty));
  $output .= '</div>';

  return $output;
}
